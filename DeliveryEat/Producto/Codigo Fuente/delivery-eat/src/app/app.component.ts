import { Component, ViewChild, OnInit } from '@angular/core';
import { FechaEntregaComponent } from './fecha-entrega/fecha-entrega.component';
import { LugarEntregaComponent } from './lugar-entrega/lugar-entrega.component';
import { FormaPagoComponent } from './forma-pago/forma-pago.component';
import { NgbdModalContent } from './modal/modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(FechaEntregaComponent)
  fechaEntregaComponent: FechaEntregaComponent;
  @ViewChild(LugarEntregaComponent)
  lugarEntregaComponent: LugarEntregaComponent;
  @ViewChild(FormaPagoComponent) formaPagoComponent: FormaPagoComponent;
  public comercio = 'Pizzeria Don Jose';
  public listaProductos: Productos[];
  public total: number;

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    this.listaProductos = [
      {
        id: 0,
        nombre: 'Pizza Pepperonni',
        imagen: '../assets/Pizza-con-pepperoni.jpg',
        precio: 350,
        cantidad: 1
      },
      {
        id: 1,
        nombre: 'Pizza Rucula',
        imagen: '../assets/rucula.jpg',
        precio: 330,
        cantidad: 1
      }
    ];
  }
  public confirmar() {
    if (!this.formaPagoComponent.tarjeta && this.formaPagoComponent.form.get('efectivo').value < this.total) {
      this.open('El monto ingresado es inferior al total');
      return;
    }
    if (this.total <= 0) {
      this.open('No posee productos en carrito');
      return;
    }
    if (!this.fechaEntregaComponent.getFormValid()) {
      this.open('La fecha es invalida');
      return;
    }
    if (!this.lugarEntregaComponent.getFormValid()) {
      this.open('Hay errores en el formulario de lugar de entrega');
      return;
    }
    if (!this.formaPagoComponent.getFormValid()) {
      this.open('Hay errores en la forma de pago');
      return;
    }

    this.open('Se ha realizado tu pedido!');
  }

  open(message) {
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.message = message;
  }

  public getTotal() {
    this.total = 0;
    this.listaProductos.forEach(item => {
      this.total += item.cantidad * item.precio;
    });
    return this.total;
  }
  public quitarItem(item) {
    this.listaProductos.splice(this.listaProductos.indexOf(item), 1);
  }
}

class Productos {
  id: number;
  nombre: string;
  imagen: string;
  precio: number;
  cantidad: number;
}
