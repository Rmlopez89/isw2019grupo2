import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-fecha-entrega',
  templateUrl: './fecha-entrega.component.html',
  styleUrls: ['./fecha-entrega.component.scss']
})
export class FechaEntregaComponent implements OnInit {
  public otraFecha = false;
  public fecha;
  public form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.crearForm();
    this.fecha = new Date();
  }

  public crearForm() {
    this.form = this.fb.group({
      fechaEntrega: ['', Validators.compose([Validators.required])],
      horaMin: ['',Validators.compose([Validators.required])]
    });
  }

  public getFormValid() {
    if (this.otraFecha) {
      return this.form.valid;
    }
    return true;
  }
}
