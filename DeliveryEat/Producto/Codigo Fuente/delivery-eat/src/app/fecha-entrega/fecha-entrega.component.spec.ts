import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FechaEntregaComponent } from './fecha-entrega.component';

describe('FechaEntregaComponent', () => {
  let component: FechaEntregaComponent;
  let fixture: ComponentFixture<FechaEntregaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FechaEntregaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FechaEntregaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
