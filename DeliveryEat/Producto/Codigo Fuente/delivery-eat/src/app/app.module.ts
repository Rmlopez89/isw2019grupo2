import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LugarEntregaComponent} from './lugar-entrega/lugar-entrega.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FormaPagoComponent} from './forma-pago/forma-pago.component';
import {FechaEntregaComponent} from './fecha-entrega/fecha-entrega.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbdModalContent} from './modal/modal.component';

@NgModule({
    declarations: [
        AppComponent,
        LugarEntregaComponent,
        FormaPagoComponent,
        FechaEntregaComponent,
        NgbdModalContent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        NgbModule,
    ],
    providers: [],
    entryComponents: [NgbdModalContent],
    bootstrap: [AppComponent]
})
export class AppModule {
}
