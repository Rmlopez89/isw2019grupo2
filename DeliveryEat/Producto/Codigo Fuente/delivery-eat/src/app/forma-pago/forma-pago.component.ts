import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-forma-pago',
  templateUrl: './forma-pago.component.html',
  styleUrls: ['./forma-pago.component.scss']
})
export class FormaPagoComponent implements OnInit {
  public tarjeta = false;
  public form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.crearForm();
  }

  public crearForm() {
    this.form = this.fb.group({
      efectivo: [
        '',
        Validators.compose([ Validators.pattern('[0-9]*')])
      ],
      numeroTarjeta: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('4[0-9]{12}(?:[0-9]{3})?$')
        ])
      ],
      nombre: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z ]*$')
        ])
      ],
      apellido: [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z ]*$')
        ])
      ],
      mesVencimiento: [
        '',
        Validators.compose([
          Validators.pattern('^(0?[1-9]|1[012])$'),
          Validators.required
        ])
      ],
      añoVencimiento: [
        '',
        Validators.compose([
          Validators.min(2019),
          Validators.pattern('[0-9]*'),
          Validators.required
        ])
      ],
      cvc: [
        '',
        Validators.compose([
          Validators.minLength(3),
          Validators.pattern('[0-9]*'),
          Validators.required
        ])
      ]
    });
  }

  public getFormValid() {
    if (this.tarjeta) {
      return this.form.valid;
    } else {
      if (
        this.form.get('efectivo').value &&
        this.form.get('efectivo').value > 0
      ) {
        return true;
      } else {
        return false;
      }
    }
  }
}
