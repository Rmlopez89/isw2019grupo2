import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LugarEntregaComponent } from './lugar-entrega.component';

describe('LugarEntregaComponent', () => {
  let component: LugarEntregaComponent;
  let fixture: ComponentFixture<LugarEntregaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LugarEntregaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LugarEntregaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
