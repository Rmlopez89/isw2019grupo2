import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-lugar-entrega',
    templateUrl: './lugar-entrega.component.html',
    styleUrls: ['./lugar-entrega.component.scss']
})
export class LugarEntregaComponent implements OnInit {

    public form: FormGroup;

    constructor(private fb: FormBuilder) {
    }

    ngOnInit() {
        this.crearForm();
    }

    public crearForm() {
        this.form = this.fb.group({
            calle: ['', Validators.compose([
              Validators.required,
              Validators.pattern('^[a-zA-Z ]*$')
            ])],
            numero: ['', Validators.compose([
              Validators.pattern('[0-9]*'),
              Validators.required
            ])],
            ciudad: ['', Validators.required],
            referencia: [],
        })
    }

    public getFormValid() {
        return this.form.valid;
    }

}
